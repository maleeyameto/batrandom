# batrandom

<div align="center">
   <img align="center" src="https://gitlab.com/cervoneluca/batrandom/-/raw/master/assets/the-bat.png" width="250px" alt="the bat" title="the bat">
</div>


A node package that creates random bat-things 

## Install

<img align="center" src="https://gitlab.com/cervoneluca/openbits/-/raw/da8cb1517f1ef7cdb482e63c6a3ed6b96fed2205/advertising-material/images/logos/logo-new/icon/logo-icon-black.png" height="50px" alt="OpenBits logo" title="OpenBits Logo"> This package is [only] served through [OpenBits]((https://openbits.world)). 

To install OpenBits run: 

```shell
npm install openbits -g
```

Then login into openbits by following these <a href="https://www.npmjs.com/package/openbits" target="_blank">instructions</a>.

When OpenBits is set up, then install batrandom as followings: 

```shell
openbits install batrandom
```

## Usage

Import the package: 

```Javascript
import batRandom from 'batrandom';
```

Create a bat-thing by doing the following:

```javascript
const batThing = batRandom('noun');
console.log(batThing);

// will print bat-something

const batThing = batRandom('adjective');
console.log(batThing);

// will print bat-somehow
```



## Contribute

If you really want to contribute to this project, you need to tell me the answer to all questions. 


